# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.2.0] 2024-12-08
### Added
* Made more information optional to support more servers
* Added a function that tries to convert dates to chrono object

## [0.1.0] 2021-04-10
### Added
* Support for pulling 2 main structs
