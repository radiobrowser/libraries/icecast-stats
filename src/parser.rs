#[cfg(feature = "chrono")]
use chrono::{DateTime, FixedOffset};

#[cfg(feature = "chrono")]
pub fn parse_datetime(input: &str) -> Result<DateTime<FixedOffset>, chrono::ParseError> {
    let formats = [
        "%a, %d %b %Y %H:%M:%S %z",      // Example: "Fri, 06 Dec 2024 12:32:00 +0200"
        "%d/%b/%Y:%H:%M:%S %z",          // Example: "18/Nov/2024:21:11:35 +0000"
        "%Y-%m-%dT%H:%M:%S%z",           // Example: "2024-12-06T12:32:00+0200"
    ];

    // Try custom formats first
    for format in &formats {
        if let Ok(parsed) = DateTime::parse_from_str(input, format) {
            return Ok(parsed);
        }
    }

    // Fallback to RFC 3339 (ISO 8601) format
    if let Ok(parsed) = DateTime::parse_from_rfc3339(input) {
        return Ok(parsed);
    }

    // Fallback to RFC 2822 format
    DateTime::parse_from_rfc2822(input)
}