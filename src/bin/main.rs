use reqwest::blocking::get;
use std::env;
use std::error::Error;
use url::Url;

use icecast_stats::{generate_icecast_stats_url, IcecastStatsRoot};

fn main() -> Result<(), Box<dyn Error>> {
    let args: Vec<String> = env::args().collect();
    if args.len() != 2 {
        println!("url parameter expected");
        std::process::exit(1);
    } else {
        println!("* Parsing arg as url '{}'", args[1]);
        let base_url = Url::parse(&args[1])?;
        println!("* Create icecast status url from '{}'", base_url);
        let url = generate_icecast_stats_url(base_url);
        println!("* Fetching from '{}'", url);
        let resp = get(url.to_string())?;
        println!("* Decode json from response");
        let j: IcecastStatsRoot = resp.json()?;
        println!("* Resultlist below\n");
        println!("ServerID: {}", j.icestats.server_id());
        println!("Location: {}", j.icestats.location());
        println!("Admin contact: {}", j.icestats.admin());
        #[cfg(not(feature = "chrono"))]
        println!("Server start: {:?}", j.icestats.server_start());
        #[cfg(feature = "chrono")]
        println!("Server start: {:?}", j.icestats.server_start_decoded());

        let streams = j.icestats.sources_vec();

        for stream in streams {
            println!("- {}", stream.generate_name());
            println!("  {}", stream.listenurl().unwrap_or(&String::new()));
            #[cfg(not(feature = "chrono"))]
            println!("  {:?}", stream.stream_start());
            #[cfg(feature = "chrono")]
            println!("  {:?}", stream.stream_start_decoded());
        }
    }
    Ok(())
}
