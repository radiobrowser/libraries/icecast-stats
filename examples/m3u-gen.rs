use reqwest::blocking::get;
use std::env;
use std::error::Error;
use std::fs::File;
use std::io::Write;
use url::Url;

use icecast_stats::{generate_icecast_stats_url, IcecastStatsRoot};

fn main() -> Result<(), Box<dyn Error>> {
    let args: Vec<String> = env::args().collect();
    if args.len() != 2 {
        println!("url parameter expected");
        std::process::exit(1);
    } else {
        println!("* Parsing arg as url '{}'", args[1]);
        let base_url = Url::parse(&args[1])?;
        println!("* Create icecast status url from '{}'", base_url);
        let url = generate_icecast_stats_url(base_url);
        println!("* Fetching from '{}'", url);
        let resp = get(url.to_string())?;
        println!("* Decode json from response");
        let j: IcecastStatsRoot = resp.json()?;
        println!(
            "* Result: ServerID='{}' written to 'output.m3u'",
            j.icestats.server_id()
        );

        let mut file = File::create("output.m3u")?;
        writeln!(file, "#EXTM3U")?;

        // Write each line to the file
        for stream in j.icestats.sources_vec() {
            match stream.listenurl() {
                Some(listenurl) => {
                    let line = stream.generate_name();
                    writeln!(file, "#EXTINF:0, {}", line)?;
                    writeln!(file, "{}\n", listenurl)?;
                }
                None => println!("Ignoring station because empty listenurl"),
            }
        }
    }
    Ok(())
}
